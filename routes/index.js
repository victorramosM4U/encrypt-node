const express = require('express');
const router = express.Router();
const CryptoJS = require("crypto-js");


router.get('/', function (req, res, next) {
  var input = req.query.p;

  //Chaves
  var key = "2e35f242a46d67eeb74aabc37d5e5d06";
  var iv = "b74aabc37d5e5d052e35f242a46d67ef";

  key = CryptoJS.enc.Base64.parse(key);
  iv = CryptoJS.enc.Base64.parse(iv);

  var bytes = CryptoJS.AES.encrypt(input, key, {iv : iv});
  var cipher_info_crypto = encodeURIComponent(bytes);

  res.render('index', {string: cipher_info_crypto});
});

router.get('/decrypt', function (req, res, next) {
  var input = req.query.p;

  var data = decodeURIComponent(input);

  //Chaves
  var key = "2e35f242a46d67eeb74aabc37d5e5d06";
  var iv = "b74aabc37d5e5d052e35f242a46d67ef";

  key = CryptoJS.enc.Base64.parse(key);
  iv = CryptoJS.enc.Base64.parse(iv);

  var bytes = CryptoJS.AES.decrypt(data, key, { iv: iv });

  res.render('index', {string: bytes.toString(CryptoJS.enc.Utf8)});
});

module.exports = router;
